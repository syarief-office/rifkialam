/* Author:http://www.rainatspace.com

*/

function initializeScript(){

	// NAVIGATION RESPOSNIVE HANDLER
	jQuery(".menu-scroll").clone(false).appendTo(".nav-mobile");
	jQuery(window).on('load', function(){
		jQuery('.nav-mobile').find('ul').removeClass();
	});
	jQuery(".hamburger").click( function() {
		jQuery(this).toggleClass('is-active');
		jQuery(".nav-mobile").toggleClass("sidebar-active");
	});

	var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
	  $('a[data-modal-id]').click(function(e) {
	  	var modalBox = $(this).attr('data-modal-id'),
	  		targetScroll = $('#'+modalBox);
	    e.preventDefault();
	    $("body").append(appendthis);
	    $(".modal-overlay").fadeTo(200, 0.9);
	    //$(".js-modalbox").fadeIn(500);
	    $('.modal-box').hide();
	    $('#'+modalBox).fadeIn($(this).data());

	    $('html, body').animate({
	    	scrollTop : targetScroll.offset().top
	    }, 1000);

	  });  
 
	$(".js-modal-close, .modal-overlay").click(function() {
	    $(".modal-box, .modal-overlay").slideUp(200, function() {
	        //$(".modal-overlay").remove();
	    });
	 
	});
	//MAKE THE MODAL CENTERED
	$(window).resize(function() {
	    $(".modal-box").css({
	        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
	        left: ($(window).width() - $(".modal-box").outerWidth()) / 2,
	        overflow: 'auto'
	    });
	});
	$(window).resize();
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
    
    var winWidth = jQuery(window).width();
    jQuery(window).bind('resize, load', function(){
		if(winWidth > 982){
			jQuery('#scrollingarea').pagepiling({
				menu: '.menuScroll',
				anchors: ['page1', 'page2', 'page3'],
			    sectionsColor: ['#ffffff', '#2ebe21', '#2C3E50', '#51bec4'],
			    navigation: {
		            position: 'left'
		        },
			    onLeave: function(index, nextIndex, direction){
			    },
			    afterRender: function(){
			    	//$('#callbacksDiv').append('<p>afterRender</p>');
			    },
			    afterLoad: function(anchorLink, index){
			    	//$('#callbacksDiv').append('<p>afterLoad - anchorLink:' + anchorLink + " index:" + index + '</p>');

			    	//section 2
					/*if(index == 1){
						//moving the image
						$('.section1').find('.hero').delay(100).animate({
							left: '0%'
						}, 1500, 'easeOutExpo', function(){
							$('.section1').find('h1').first().fadeIn(700, function(){
								$('.section1').find('h1').last().fadeIn(600);
							});
						});
					}
					//section 3
					/*if(anchorLink == 'page3'){
						//section 3
						$('#section3').find('.intro').delay(100).animate({
								left: '0%'
						}, 1500, 'easeOutExpo');
					}

					deleteLog = true;*/
			    }
			}); 
    	}
  		//this.location.reload(false);
    });

    jQuery('.gridWall').masonry({
		itemSelector: '.grid-wallitem',
		percentPosition: true
		//columnWidth: 130
	});	

	//STYLE SELECT
	jQuery('select.selectStyle').customSelect();
});
/* END ------------------------------------------------------- */